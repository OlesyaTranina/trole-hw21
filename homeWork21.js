//1 задача
console.log("***** задача 1");
for (i = 2; i < 100; i+=2) {
	console.log(i);
};


//2 задача
console.log("***** задача 2");
var age=22;
if (age <= 17) {
	console.log("вы слишком молоды");
} else if (age > 17 && age <= 60) {
	console.log("отличный возраст");
} else {
	console.log("привет, бабуля!");
};

//3 задача
console.log("***** задача 3");
var date = "08.03";
switch(date) {
	case "08.03"	:
	  console.log("с 8 марта!");
	  break;
	case "23.02":
	  console.log("с 23 февраля");
	  break;
	case "31.12" :
	  console.log("с новым годом");
	  break;
	default:
	  console.log("сегодня просто отличный день и безо всякого праздника;");
}

